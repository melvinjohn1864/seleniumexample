import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AnimalWebsiteTest {
	//Constants
	WebDriver driver;
	
	final String CHROME_DRIVER_LOCATION = "/Users/owner/Desktop/chromedriver";

	@Before
	public void setUp() throws Exception {
		// Selenium setup
		
		System.setProperty("webdriver.chrome.driver",CHROME_DRIVER_LOCATION);
		driver = new ChromeDriver();
		
		// 2. Enter the website you want to go to
		String baseUrl = "https://www.webdirectory.com/Animals/";
		
		// 3. Open Chrome and go to the base url;
		driver.get(baseUrl);
	}
	

	@After
	public void tearDown() throws Exception {
		
		// 7. Close the browser
				Thread.sleep(2000);  //pause for 1 second before closing the browser
				driver.close();
	}
	
	@Test
	public void testNumberofLinks() {
		
		//1. Get all the bulleted links in website
		List<WebElement> bulletLinks = driver.findElements(By.cssSelector("table+ul li a"));
		
		System.out.println("Number of links on page: " + bulletLinks.size());
		
		//Solution 2
//		List<WebElement> uls = driver.findElements(By.cssSelector("ul"));
//		
//		WebElement firstUl = uls.get(0);
//		
//		List<WebElement> bulletedLinks = firstUl.findElements(By.cssSelector("li a"));
		
		
		
		
		// Output the  links to the screen using system.print.out.ln
		
		for (int i = 0; i < bulletLinks.size(); i++) {
			WebElement link = bulletLinks.get(i);
			
			
			// Get the link text
			String linkText = link.getText();
			
			//Print the urls
			System.out.println(linkText + ": " + link.getAttribute("href"));
		}
		
		//2. Count the lines
		int totallinks = bulletLinks.size();
		
		//3. Check if lines =10
		assertEquals(10, totallinks);
		

		
	}

}
