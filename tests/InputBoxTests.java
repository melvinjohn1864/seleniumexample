import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class InputBoxTests {
	
	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		
		System.setProperty("webdriver.chrome.driver","/Users/owner/Desktop/chromedriver");
		driver = new ChromeDriver();
		
		// 2. Enter the website you want to go to
		String baseUrl = "https://www.seleniumeasy.com/test/basic-first-form-demo.html";
		
		// 3. Open Chrome and go to the base url;
		driver.get(baseUrl);
	}

	@After
	public void tearDown() throws Exception {
		
		// 7. Close the browser
		Thread.sleep(2000);  //pause for 1 second before closing the browser
		driver.close();
	}
	
	
    //Test single input field functionality
	@Test
	public void testSingleInputField() throws InterruptedException {
		//1. Enter some value into text box
		//2. Click the button
		//3. Get the actual output from the screen
		//4. Check actual output = expected output
		
		// 1. Configure Selenium to talk to Chrome
		
			
				// 4. Enter a username
				// ---------------------
				// 4a. Find the box
				WebElement inputBox = driver.findElement(By.id("user-message"));
				// 4b. Put the input in there
				inputBox.sendKeys("here is some nonsense");

				// Get the submit button and click automatically
				WebElement showMessageButton = driver.findElement(By.cssSelector("form#get-input button"));
				// 5b. Press the button
				showMessageButton.click();
				
				WebElement outputBox = driver.findElement(By.id("display"));
				String output = outputBox.getText();
				
				assertEquals("here is some nonsense",output);
			

	}
	
	
	
	//Test 2 input field functionality
	@Test
	public void testTwoInputField() throws InterruptedException {
		// ---------------------
		// 4a. Find the box
		WebElement firstInputBox = driver.findElement(By.id("sum1"));
		// 4b. Put the input in there
		firstInputBox.sendKeys("100");
		
		WebElement secondInputBox = driver.findElement(By.id("sum2"));
		// 4b. Put the input in there
		secondInputBox.sendKeys("200");

		// Get the total button and click automatically
		WebElement getTotalButton = driver.findElement(By.cssSelector("form#gettotal button"));
		// 5b. Press the button
		getTotalButton.click();
		
		WebElement outputBox = driver.findElement(By.id("displayvalue"));
		String output = outputBox.getText();
		
		assertEquals("300",output);
		
	}

}
